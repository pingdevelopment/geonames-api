<?php
namespace PingDevelopment\Geonames;

use PingDevelopment\Parsers\XML2Array;
use PingDevelopment\Parsers\XMLParser;

/**
 * ApiWrapperBase.php
 * @author  Peter Adams
 * http://www.pingdevelopment.com
 * Date Created: 09 September 2014
 *
 * Copyright (c) 2014 Ping Enterprises LLC. All rights reserved.
 *
 * This code is provided by Ping Enterprises LLC (dba Ping! Development) with a perpetual,
 * non-exclusive, royalty-free license.
 *
 */
abstract class GeonamesBase
{
    const LIVE_API_URL = "http://api.geonames.org/";

    const PARAMETER_USERNAME = "username";

    const OPERATION_SUFFIX_JSON = "JSON";
    const DATA_FORMAT_JSON = "json";
    const DATA_FORMAT_XML = "xml";

    const CODE_FEATURE_BUILDING = "BLDG";
    const CODE_FEATURE_POPULATED_PLACE = "PPL";
    const CODE_FEATURE_FIRST_LEVEL = "ADM1";
    const CODE_FEATURE_SECOND_LEVEL = "ADM2";
    const CODE_FEATURE_THIRD_LEVEL = "ADM3";

  /**
   *  the error code if one exists
   * @var integer
   */
  protected $errorCode = 0;

  /**
   * the error message if one exists
   * @var string
   */
  protected $errorMessage = '';

  /**
   *  the response message
   * @var string
   */
  protected $response = '';

  /**
   * The format of the response data (Default is 'json').
   *
   * @var string
   */
  protected $responseFormat = "json";

  /**
   *  the headers returned from the call made
   * @var array
   */
  protected $headers = '';

  /**
   * The response represented as an array
   * @var array
   */
  protected $arrayResponse = array();

  /**
   * Query parameters as $key => $value
   *
   * @var array
   */
  protected $parameters = array();

  /**
   * Default options for curl.
   */
  public static $CURL_OPTS = array(
    CURLOPT_CONNECTTIMEOUT => 30,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_TIMEOUT        => 60,
    CURLOPT_FRESH_CONNECT  => 1,
    CURLOPT_PORT           => 443,
    CURLOPT_USERAGENT      => 'usps-php',
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_RETURNTRANSFER => true,
  );

  /**
   * Constructor
   */
  public function __construct()
  {
  }

  /**
   * Get the URL of the API to use.
   *
   * @return string
   */
  abstract public function getEndpoint();

  /**
   * Build the query string from the parameters.
   *
   * @return string
   */
  abstract public function getQueryString();

  /**
   * Return the post data fields as an array
   *
   * @return array
   */
  public function getParameters()
  {
      return $this->parameters;
  }

  /**
   * Return the post data fields as an array
   *
   * @param $parameters
   *
   * @return array
   */
  public function setParameters($parameters)
  {
      $this->parameters = $parameters;

      return $this;
  }

  /**
   * @return string
   */
  public function getResponseFormat()
  {
      return $this->responseFormat;
  }

  /**
   * @param string $format
   *
   * @return $this
   */
  public function setResponseFormat($format)
  {
      $this->responseFormat = trim($format);

      return $this;
  }

  /**
   * Get the username for the request.
   *
   * @return string
   */
  public function getUsername()
  {
      $parameters = $this->getParameters();
      if (!isset($parameters[self::PARAMETER_USERNAME])) {
          return "";
      }

      return $parameters[self::PARAMETER_USERNAME];
  }

  /**
   * Add a parameter to specify the username.
   *
   * @param $username
   */
  public function setUsername($username)
  {
      $parameters = $this->getParameters();
      $parameters[self::PARAMETER_USERNAME] = $username;

      $this->setParameters($parameters);
  }

  /**
   * Makes an HTTP request. This method can be overriden by subclasses if
   * developers want to do fancier things or use something other than curl to
   * make the request.
   *
   * @param CurlHandler optional initialized curl handle
   *
   * @return String the response text
   */
  protected function doRequest($ch = null)
  {
      if (!$ch) {
          $ch = curl_init();
      }

      $opts = self::$CURL_OPTS;
      $opts[ CURLOPT_URL ] = $this->getEndpoint();

    // Replace 443 with 80 if it's not secured
    if (strpos($opts[ CURLOPT_URL ], 'https://') === false) {
        $opts[ CURLOPT_PORT ] = 80;
    }

    // set options
    curl_setopt_array($ch, $opts);

    // execute
    $this->setResponse(curl_exec($ch));
      $this->setHeaders(curl_getinfo($ch));

    // fetch errors
    $this->setErrorCode(curl_errno($ch));
      $this->setErrorMessage(curl_error($ch));

    // Convert response to array
    $this->convertResponseToArray();

    // If it failed then set error code and message
    if ($this->isError()) {
        $arrayResponse = $this->getArrayResponse();

      // Find the error number
      $errorInfo = $this->getValueByKey($arrayResponse, 'Error');

        if ($errorInfo) {
            $this->setErrorCode($errorInfo['Number']);
            $this->setErrorMessage($errorInfo['Description']);
        }
    }

    // close
    curl_close($ch);

      return $this->getResponse();
  }

  /**
   * Return the xml string built that we are about to send over to the api
   * @return string
   */
  protected function getXMLString()
  {
      $xml = XMLParser::createXML("request", $this->getParameters());

      return $xml->saveXML();
  }

  /**
   * Did we encounter an error?
   * @return boolean
   */
  public function isError()
  {
      $headers = $this->getHeaders();
      $response = $this->getArrayResponse();
    // First make sure we got a valid response
    if ($headers['http_code'] != 200) {
        return true;
    }

    // No error
    return false;
  }

  /**
   * Was the last call successful
   * @return boolean
   */
  public function isSuccess()
  {
      return !$this->isError() ? true : false;
  }

  /**
   * Return the response represented as string
   * @return array
   */
  public function convertResponseToArray()
  {
      if ($this->getResponse() && ($this->getResponseFormat() === self::DATA_FORMAT_XML)) {
          $this->setArrayResponse(XML2Array::createArray($this->getResponse()));
      } elseif ($this->getResponseFormat() === self::DATA_FORMAT_JSON) {
          $this->setArrayResponse(json_decode($this->getResponse()));
      }

      return $this->getArrayResponse();
  }

  /**
   * Set the array response value
   *
   * @param array $value
   *
   * @return void
   */
  public function setArrayResponse($value)
  {
      $this->arrayResponse = (array)$value;
  }

  /**
   * Return the array representation of the last response
   * @return array
   */
  public function getArrayResponse()
  {
      return $this->arrayResponse;
  }

  /**
   * Set the response
   *
   * @param mixed the response returned from the call
   *
   * @return facebookLib object
   */
  public function setResponse($response = '')
  {
      $this->response = $response;
      $this->convertResponseToArray();

      return $this;
  }

  /**
   * Get the response data
   *
   * @return mixed the response data
   */
  public function getResponse()
  {
      return $this->response;
  }

  /**
   * Set the headers
   *
   * @param string $headers
   *
   * @internal param $headers array
   * @return facebookLib object
   */
  public function setHeaders($headers = '')
  {
      $this->headers = $headers;

      return $this;
  }

  /**
   * Get the headers
   *
   * @return array the headers returned from the call
   */
  public function getHeaders()
  {
      return $this->headers;
  }

  /**
   * Set the error code number
   *
   * @param integer the error code number
   *
   * @return facebookLib object
   */
  public function setErrorCode($code = 0)
  {
      $this->errorCode = $code;

      return $this;
  }

  /**
   * Get the error code number
   *
   * @return integer error code number
   */
  public function getErrorCode()
  {
      return $this->errorCode;
  }

  /**
   * Set the error message
   *
   * @param string the error message
   *
   * @return facebookLib object
   */
  public function setErrorMessage($message = '')
  {
      $this->errorMessage = $message;

      return $this;
  }

  /**
   * Get the error code message
   *
   * @return string error code message
   */
  public function getErrorMessage()
  {
      return $this->errorMessage;
  }

  /**
   * Find a key inside a multi dim. array
   *
   * @param array  $array
   * @param string $key
   *
   * @return mixed
   */
  protected function getValueByKey($array, $key)
  {
      foreach ($array as $k => $each) {
          if ($k == $key) {
              return $each;
          }

          if (is_array($each)) {
              if ($return = $this->getValueByKey($each, $key)) {
                  return $return;
              }
          }
      }

    // Nothing matched
    return null;
  }
}
