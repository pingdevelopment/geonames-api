<?php
namespace PingDevelopment\Geonames;

class PostalCodeSearch extends GeonamesBase
{
    const OPERATION_NAME = "postalCodeSearch";

    const FIELD_POSTAL_CODE = "postalCode";
    const FIELD_CITY = "placeName";
    const FIELD_STATE = "adminCode1";
    const FIELD_LATITUDE = "lat";
    const FIELD_LONGITUDE = "lng";

    const FILTER_POSTAL_CODE = "postalcode";
    const FILTER_POSTAL_CODE_STARTS_WITH = "postalcode_startsWith";
    const FILTER_PLACE_NAME = "placename";
    const FILTER_PLACE_NAME_STARTS_WITH = "placename_startsWith";
    const FILTER_STATE_CODE = "adminCode1";
    const FILTER_COUNTRY = "country";
    const FILTER_COUNTRY_BIAS = "countryBias";

    const PARAMETER_MAX_ROWS = "maxRows";

  /**
   * Get the URL of the API to use.
   *
   * @return string
   */
  public function getEndpoint()
  {
      return GeonamesBase::LIVE_API_URL .
           self::OPERATION_NAME . (($this->getResponseFormat() === GeonamesBase::DATA_FORMAT_JSON) ? GeonamesBase::OPERATION_SUFFIX_JSON : "") .
           "?" . $this->getQueryString();
  }

  /**
   * Get the query string from the list of parameters.
   *
   * @return string
   */
  public function getQueryString()
  {
      $queryString = "";
      $parameters = $this->getParameters();
      foreach ($parameters as $key => $value) {
          $queryString .= $key."=".$value."&";
      }

      return trim($queryString, "& ");
  }

  /**
   * Perform the API call
   * @return string
   */
  public function lookup()
  {
      return $this->doRequest();
  }

  /**
   * Add a filter to the query.
   *
   * @param $fieldName
   * @param $value
   */
  public function addFilter($fieldName, $value)
  {
      $parameters = $this->getParameters();
      $parameters[$fieldName] = $value;

      $this->setParameters($parameters);
  }

  /**
   * @param $fieldName
   */
  public function removeFilter($fieldName)
  {
      $parameters = $this->getParameters();
      if (isset($parameters[$fieldName])) {
          unset($parameters[$fieldName]);
      }

      $this->setParameters($parameters);
  }

  /**
   * Add a parameter to specify the maximum number of rows.
   *
   * @param $max
   */
  public function setMaxResults($max)
  {
      $parameters = $this->getParameters();
      $parameters[self::PARAMETER_MAX_ROWS] = $max;

      $this->setParameters($parameters);
  }

  /**
   * Do custom error checking before we check the parent class.
   *
   * @return boolean
   */
  public function isError()
  {
      $response = $this->getArrayResponse();

      if (!isset($response['postalCodes'])) {
          return true;
      }

      return parent::isError();
  }
}
