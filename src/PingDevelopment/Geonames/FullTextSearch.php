<?php
namespace PingDevelopment\Geonames;

class FullTextSearch extends GeonamesBase
{
    const OPERATION_NAME = "search";

    const FIELD_LATITUDE = "lat";
    const FIELD_LONGITUDE = "lng";

    const FIELD_CITY = "name";
    const FIELD_STATE = "adminName1";
    const FIELD_STATE_CODE = "adminCode1";
    const FIELD_COUNTRY = "countryName";
    const FIELD_COUNTRY_ID = "countryId";
    const FIELD_COUNTRY_CODE = "countryCode";

    const FILTER_QUERY = "q";
    const FILTER_NAME = "name";
    const FILTER_NAME_EQUALS = "name_equals";
    const FILTER_NAME_STARTS_WITH = "name_startsWith";
    const FILTER_FEATURE_CODE = "featureCode";
    const FILTER_COUNTRY = "country";
    const FILTER_COUNTRY_BIAS = "countryBias";
    const FILTER_CONTINENT_CODE = "continentCode";

    const PARAMETER_MAX_ROWS = "maxRows";
    const PARAMETER_START_ROW = "startRow";
    const PARAMETER_VERBOSITY = "style";

  /**
   * Get the URL of the API to use.
   *
   * @return string
   */
  public function getEndpoint()
  {
      return GeonamesBase::LIVE_API_URL .
           self::OPERATION_NAME . (($this->getResponseFormat() === GeonamesBase::DATA_FORMAT_JSON) ? GeonamesBase::OPERATION_SUFFIX_JSON : "") .
           "?" . $this->getQueryString();
  }

  /**
   * Get the query string from the list of parameters.
   *
   * @return string
   */
  public function getQueryString()
  {
      $queryString = "";
      $parameters = $this->getParameters();
      foreach ($parameters as $key => $value) {
          $queryString .= $key."=".$value."&";
      }

      return trim($queryString, "& ");
  }

  /**
   * Perform the API call
   * @return string
   */
  public function lookup()
  {
      return $this->doRequest();
  }

  /**
   * Add a filter to the query.
   *
   * @param $fieldName
   * @param $value
   */
  public function addFilter($fieldName, $value)
  {
      $parameters = $this->getParameters();
      $parameters[$fieldName] = $value;

      $this->setParameters($parameters);
  }

  /**
   * @param $fieldName
   */
  public function removeFilter($fieldName)
  {
      $parameters = $this->getParameters();
      if (isset($parameters[$fieldName])) {
          unset($parameters[$fieldName]);
      }

      $this->setParameters($parameters);
  }

  /**
   * Add a parameter to specify the maximum number of rows.
   *
   * @param $max
   */
  public function setMaxResults($max)
  {
      $parameters = $this->getParameters();
      $parameters[self::PARAMETER_MAX_ROWS] = $max;

      $this->setParameters($parameters);
  }

  /**
   * Add a parameter to specify the starting row.
   *
   * @param $start
   */
  public function setStartRow($start)
  {
      $parameters = $this->getParameters();
      $parameters[self::PARAMETER_START_ROW] = $start;

      $this->setParameters($parameters);
  }

  /**
   * Do custom error checking before we check the parent class.
   *
   * @return boolean
   */
  public function isError()
  {
      $response = $this->getArrayResponse();

      if (!isset($response['geonames'])) {
          return true;
      }

      return parent::isError();
  }
}
